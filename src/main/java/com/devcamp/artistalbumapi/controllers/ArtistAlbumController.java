package com.devcamp.artistalbumapi.controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.artistalbumapi.model.Album;
import com.devcamp.artistalbumapi.model.Artist;
import com.devcamp.artistalbumapi.services.AlbumService;
import com.devcamp.artistalbumapi.services.ArtistService;

@RestController
@RequestMapping("")
@CrossOrigin
public class ArtistAlbumController {
    @Autowired
    ArtistService artistService;
    @GetMapping("/artists")
    public ArrayList<Artist> getAllArtistApi(){
        return artistService.getArtistList();
    }
    
    @GetMapping("/artist-info")
    public Artist getArtistInfoApi(@RequestParam(required = true, name = "artistId") int artistId){
        ArrayList<Artist> artistList = new ArrayList<>();
        artistList.addAll(artistService.getArtistList());
        Artist artist = new Artist();
        for (int i=0; i< artistList.size(); i++){
            if (artistList.get(i).getId() == artistId){
                artist = artistList.get(i);
            }
        }
        return artist;
    }
    @Autowired
    AlbumService albumService;
    @GetMapping("/album-info")
    public Album getAlbumInfoApi(@RequestParam(required = true, name = "albumId") int albumId){
        ArrayList<Album> albumList = new ArrayList<>();
        albumList.addAll(albumService.allAlbum());
        Album album = new Album();
        for (int i = 0; i < albumList.size(); i++){
            if (albumList.get(i).getId() == albumId){
                album = albumList.get(i);
            }
        }
        return album;
    }
    @GetMapping("/artists/{index}")
    public Artist getArtist(@PathVariable int index){
        return artistService.getArtist(index);
    }
}
