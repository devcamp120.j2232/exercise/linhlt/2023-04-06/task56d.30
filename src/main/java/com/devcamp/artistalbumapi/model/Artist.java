package com.devcamp.artistalbumapi.model;

import java.util.ArrayList;

public class Artist extends Album{
    private int id;
    private String name;
    private ArrayList<Album> albums;
    public Artist() {
    }
    public Artist(int id, String name) {
        this.id = id;
        this.name = name;
    }
    public Artist(int id, String name, ArrayList<Album> albums) {
        this.id = id;
        this.name = name;
        this.albums = albums;
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public ArrayList<Album> getAlbums() {
        return albums;
    }
    public void setAlbums(ArrayList<Album> albums) {
        this.albums = albums;
    }
    @Override
    public String toString() {
        return "Artist [id=" + id + ", name=" + name + ", albums=" + albums + "]";
    }
    
}
