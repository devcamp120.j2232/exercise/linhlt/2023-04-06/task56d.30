package com.devcamp.artistalbumapi.services;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.artistalbumapi.model.Artist;
@Service
public class ArtistService extends AlbumService{
    private ArrayList<Artist> artistList = new ArrayList<>();
    public ArrayList<Artist> getArtistList(){
        Artist artist1 = new Artist(100,"Maria", albumListAuthor1());
        Artist artist2 = new Artist(200,"Chopin", albumListAuthor2());
        Artist artist3 = new Artist(300,"Son Tung", albumListAuthor3());
        artistList.add(artist1);
        artistList.add(artist2);
        artistList.add(artist3);
        return artistList;
    }
    public Artist getArtist(int index){
        Artist artist = null;
        if (index >=0 && index <= 2){
            artist = getArtistList().get(index);
        }
        return artist;
    }
}

